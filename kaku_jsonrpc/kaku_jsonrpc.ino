
#include <aJSON.h>
#include <JsonRPC.h>
#include <RemoteTransmitter.h>

JsonRPC rpc(1);
aJsonStream serial_stream(&Serial);

ActionTransmitter actionTransmitter(11);

void lightswitch(aJsonObject* params)
{
	int code = aJson.getObjectItem(params, "code")->valueint;
	char button = aJson.getObjectItem(params, "button")->valuestring[0];
	boolean value = aJson.getObjectItem(params, "value")->valuebool;

	actionTransmitter.sendSignal(code, button, value);
	actionTransmitter.sendSignal(code, button, value);
	actionTransmitter.sendSignal(code, button, value);
	actionTransmitter.sendSignal(code, button, value);
	actionTransmitter.sendSignal(code, button, value);
}

void setup()
{
	Serial.begin(115200);

	rpc.registerMethod("lightswitch", &lightswitch);

	//actionTransmitter.sendSignal(10, 'B', false);
	//actionTransmitter.sendSignal(10, 'B', true);
}

void loop()
{
	if (serial_stream.available()) {
		/* First, skip any accidental whitespace like newlines. */
		serial_stream.skip();
	}

	if (serial_stream.available()) {
		/* Something real on input, let's take a look. */
		aJsonObject *msg = aJson.parse(&serial_stream);
		rpc.processMessage(msg);
		aJson.deleteItem(msg);
	}
}
