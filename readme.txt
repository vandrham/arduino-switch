# arduino IDE
apt-get install arduino

# required libraries: (/usr/share/arduino/librabries)
- aJson (https://github.com/interactive-matter/aJson)
- JsonRPC (https://github.com/cloud-rocket/arduino-json-rpc)
- RemoteSwitch (https://bitbucket.org/fuzzillogic/433mhzforarduino)